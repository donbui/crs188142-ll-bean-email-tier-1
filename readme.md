Clone the repo and run
```
npm install
```

To watch a single .mjml file:
```
mjml --watch index.mjml -o index.html
```
